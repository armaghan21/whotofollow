
/**
 * Created by armaghan on 2/12/17.
 */
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

public class WhoToFollow2 {

    /*
    Mapper 2
     */
    public static class AllPairsMapper extends Mapper<Object, Text, IntWritable, IntWritable> {

        public void map(Object key, Text values, Context context) throws IOException, InterruptedException {
            StringTokenizer st = new StringTokenizer(values.toString());
            IntWritable user = new IntWritable(Integer.parseInt(st.nextToken()));

            ArrayList<Integer> followers = new ArrayList<>();
            ArrayList<Integer> followees = new ArrayList<>();
            // First, go through the list of all friends of user 'user' and emit
            // (user,-friend)
            // 'friend1' will be used in the emitted pair
            IntWritable follower = new IntWritable();

            while (st.hasMoreTokens()) {
                Integer f = Integer.parseInt(st.nextToken());
                if (f >= 0) {
                    followees.add(f);
                } else {
                    followers.add(f);
                }
            }

            for (Integer f : followers) {
                follower.set(f);
                context.write(user, follower);
            }

            for (Integer i : followees) {
                user.set(i);
                for (Integer j : followees) {
                    follower.set(j);
                    context.write(user, follower);
                }
            }
        }
    }

    public static class CountReducer extends Reducer<IntWritable, IntWritable, IntWritable, Text> {

        // A private class to describe a recommendation.
        // A recommendation has a follower id and a number of followers in common.
        private static class Recommendation {

            // Attributes
            private int followerId;
            private int nCommonFollowers;

            // Constructor
            public Recommendation(int followerId) {
                this.followerId = followerId;
                // A recommendation must have at least 1 common friend
                this.nCommonFollowers = 1;
            }

            // Getters
            public int getFollowerId() {
                return followerId;
            }

            public int getnCommonFollowers() {
                return nCommonFollowers;
            }

            // Other methods
            // Increments the number of common friends
            public void addCommonFriend() {
                nCommonFollowers++;
            }

            // String representation used in the reduce output
            public String toString() {
                return followerId + "(" + nCommonFollowers + ")";
            }

            // Finds a representation in an array
            public static Recommendation find(int followerId, ArrayList<Recommendation> recommendations) {
                for (Recommendation p : recommendations) {
                    if (p.getFollowerId() == followerId) {
                        return p;
                    }
                }
                // Recommendation was not found!
                return null;
            }
        }

        // The reduce method
        public void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            IntWritable user = key;
            // 'existingFriends' will store the friends of user 'user'
            // (the negative values in 'values').
            ArrayList<Integer> existingFriends = new ArrayList();
            // 'recommendedUsers' will store the list of user ids recommended
            // to user 'user'
            ArrayList<Integer> recommendedUsers = new ArrayList<>();
            while (values.iterator().hasNext()) {
                int value = values.iterator().next().get();
                if (value > 0) {
                    recommendedUsers.add(value);
                } else {
                    existingFriends.add(value);
                }
            }
            // 'recommendedUsers' now contains all the positive values in 'values'.
            // We need to remove from it every value -x where x is in existingFriends.
            // See javadoc on Predicate: https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html
            for (Integer follower : existingFriends) {
                recommendedUsers.removeIf(new Predicate<Integer>() {
                    @Override
                    public boolean test(Integer t) {
                        return t.intValue() == -follower.intValue() || t.intValue() == user.get();
                    }
                });
            }
            ArrayList<Recommendation> recommendations = new ArrayList<>();
            // Builds the recommendation array
            for (Integer userId : recommendedUsers) {
                Recommendation p = Recommendation.find(userId, recommendations);
                if (p == null) {
                    recommendations.add(new Recommendation(userId));
                } else {
                    p.addCommonFriend();
                }
            }
            // Sorts the recommendation array
            recommendations.sort(new Comparator<Recommendation>() {
                @Override
                public int compare(Recommendation t, Recommendation t1) {
                    return -Integer.compare(t.getnCommonFollowers(), t1.getnCommonFollowers());
                }
            });
            // Builds the output string that will be emitted
            StringBuffer sb = new StringBuffer(""); // Using a StringBuffer is more efficient than concatenating strings
            for (int i = 0; i < recommendations.size() && i < 10; i++) {
                Recommendation p = recommendations.get(i);
                sb.append(p.toString() + " ");
            }
            Text result = new Text(sb.toString());
            context.write(user, result);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "task 2");
        job.setJarByClass(WhoToFollow2.class);
        job.setMapperClass(AllPairsMapper.class);
        job.setReducerClass(CountReducer.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
