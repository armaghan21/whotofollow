

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class ChainJobs extends Configured implements Tool {

    private static final String OUTPUT_PATH = "intermediate_output";

    @Override
    public int run(String[] args) throws Exception {
  /*
   * Job 1
   */
        Configuration conf = getConf();
        Job job = Job.getInstance(conf, "task 1");
        job.setJarByClass(WhoToFollow1.class);

        job.setMapperClass(WhoToFollow1.AllPairsMapper.class);
        job.setReducerClass(WhoToFollow1.CountReducer.class);

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(IntWritable.class);

        TextInputFormat.addInputPath(job, new Path(args[0]));
        TextOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH));

        job.waitForCompletion(true);

  /*
   * Job 2
   */

        Job job2 = Job.getInstance(conf, "task 2");
        job2.setJarByClass(WhoToFollow2.class);

        job2.setMapperClass(WhoToFollow2.AllPairsMapper.class);
        job2.setReducerClass(WhoToFollow2.CountReducer.class);

        job2.setOutputKeyClass(IntWritable.class);
        job2.setOutputValueClass(IntWritable.class);

        job2.setInputFormatClass(TextInputFormat.class);
        job2.setOutputFormatClass(TextOutputFormat.class);

        TextInputFormat.addInputPath(job2, new Path(OUTPUT_PATH));
        TextOutputFormat.setOutputPath(job2, new Path(args[1]));


        boolean res =  job2.waitForCompletion(true);

        FileSystem hdfs = FileSystem.get(conf);
        Path intermediate = new Path(OUTPUT_PATH);
        // delete existing directory
        if (hdfs.exists(intermediate)) {
            hdfs.delete(intermediate, true);
        }

        return res ? 0 : 1;
    }

    /**
     * Method Name: main Return type: none Purpose:Read the arguments from
     * command line and run the Job till completion
     *
     */
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Enter valid number of arguments <Inputdirectory>  <Outputlocation>");
            System.exit(0);
        }
        ToolRunner.run(new Configuration(), new ChainJobs(), args);
    }
}
