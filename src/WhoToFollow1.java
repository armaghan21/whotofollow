
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class WhoToFollow1 {


    /*
    First mapper
     */
    public static class AllPairsMapper extends Mapper<Object, Text, IntWritable, IntWritable> {

        public void map(Object key, Text values, Context context) throws IOException, InterruptedException {
            StringTokenizer st = new StringTokenizer(values.toString());
            IntWritable user = new IntWritable(Integer.parseInt(st.nextToken()));

            ArrayList<Integer> followers = new ArrayList<>();


            IntWritable follower = new IntWritable();
            while (st.hasMoreTokens()) {
                Integer f = Integer.parseInt(st.nextToken());
                follower.set(-f);
                context.write(user, follower);
                followers.add(f);
            }

            for (Integer f : followers) {
                follower.set(f);
                context.write(follower,user);
            }
        }
    }


    public static class CountReducer extends Reducer<IntWritable, IntWritable, IntWritable, Text> {

        // The reduce method
        public void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

            StringBuffer sb = new StringBuffer();
            while(values.iterator().hasNext()) {
                sb.append(values.iterator().next() + " ");
            }
            Text result = new Text(sb.toString());
            context.write(key, result);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "task 1");
        job.setJarByClass(WhoToFollow1.class);
        job.setMapperClass(AllPairsMapper.class);
        job.setReducerClass(CountReducer.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
