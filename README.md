# README #

### Steps to run the program ###

* Put input.txt file containing input data in hdfs
* Extract "WhoToFollow.tar.gz"
* In the terminal navigate to the following folder containing "ChainJobs.jar" file <.../WhoToFollow/out/production/WhoToFollow/>

* Make sure Hadoop dfs and yarn daemons are running
* Run the MapReduce jobs by running the following command 

"hadoop jar ChainJobs.jar ChainJobs <input.txt> <output>"

where <input.txt> is the input file present in hdfs
and <output> is the output directory created in hdfs for results

* To view the output run the following command in terminal 

"hdfs dfs -cat <path/to/output/directory>/part-r-00000"

### Implementation details ###

The algorithm includes two mappers and two reducers described as follows

"WhoToFollow1.java" class contains the first mapper and first reducer classes
"WhoToFollow2.java" class contains the first mapper and first reducer classes

The output of the first MapReduce job is being used by second MapReduce job

"ChainJobs.java" class runs both jobs in a chain.